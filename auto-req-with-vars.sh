#!/usr/bin/env bash

scp -r ./configs-for-RG-mock/${1} popova@10.10.40.17:/tmp/
ssh popova@10.10.40.17 "sudo cp /tmp/${1}/reportgw-app-mock.conf /opt/docker/nginx/data/nginx/conf.d/; docker exec -t nginx nginx -s reload"
echo ${1} mock
echo ${2} scenario env
echo ${3} folder
newman run ./auto-requests-run-by-Newman/RG-test-playground-auto-for-Newman.postman_collection.json -e ./env-for-auto-requests-run-by-Newman/Auto-${2}-scenario.postman_environment.json  --folder ${3} --reporters 'cli,json' --reporter-json-export outputfile.json
echo ${1} mock
echo ${2} scenario env
echo ${3} folder
echo end